import { Action } from '@ngrx/store';

export enum MyActionTypes {
    MyAction = 'My Action',
    MySecondAction = 'My Second Action'
}

export class MyAction implements Action {
    readonly type = MyActionTypes.MyAction;

    constructor(public payload: string) {}
}

export class MySecondAction implements Action {
    readonly type = MyActionTypes.MySecondAction;
}

export type MyActionsUnion =
  | MyAction
  | MySecondAction;
