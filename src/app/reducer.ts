import { SetValueAction } from 'ngrx-forms';
import { MyActionsUnion, MyActionTypes } from './actions';

interface State {
    foo: string;
}

const initialState: State = {
    foo: '',
};

export function reducer(state = initialState, action: MyActionsUnion | SetValueAction<any>) {
    switch (action.type) {
        case MyActionTypes.MyAction:
            console.log(action.payload); // <<< TS ERROR!
            return state;
        case MyActionTypes.MySecondAction:
            console.log('no payload', action);
            return state;
        default:
            return state;
    }
}
